let mix = require('laravel-mix');

mix
  .copy("frontend/dist/spa/index.html", "resources/views/app.blade.php")
  .copyDirectory("frontend/dist/spa", "public");